<?php
/**
 * Plugin Name: Woo Products XML Upload
 * Description: Give a possibility to upload xml file with products and add it to DB.
 * Version: 1.0
 * Author: Serg
 */

/**
 * Add page to admin menu.
 */
function current_admin_menu() {
	add_menu_page( __( 'Woocommerce Upload XML' ), __( 'Woocommerce Upload XML' ), 'manage_options', 'woo_products_xml_upload/woo-products-xml-upload.php', 'woo_products_xml_upload_admin_page', 'dashicons-media-spreadsheet', 6 );
}

/**
 * Add form to Woocommerce Upload XML.
 */
function woo_products_xml_upload_admin_page() {
	include plugin_dir_path( __FILE__ ) . 'templates/upload-form.php';
}

add_action( 'admin_menu', 'current_admin_menu' );

/**
 * Get id of the image using src attribute.
 */
function get_attachment_id_from_src( $image_src ) {
	global $wpdb;
	$id = $wpdb->get_var( $wpdb->prepare( "SELECT ID FROM {$wpdb->posts} WHERE guid=%s", $image_src ) );
	return $id;
}

/**
 * Add error if something went wrong.
 */
function woo_products_admin_notice_success() {
	$message = __( 'Products were uploaded successfully.' );
	print '<div id="message" class="updated"><p>' . $message . '</p></div>';
}

/**
 * Add success message after correct uploading.
 */
function woo_products_admin_notice_error() {
	$message = __( 'Something went wrong. Please check chosen file.' );
	print '<div id="message" class="error"><p>' . $message . '</p></div>';
}

/**
 * Create new posts using XML and push it to DB.
 */
try {
	function woo_products_xml_upload() {
		require_once( ABSPATH . 'wp-admin/includes/media.php' );
		require_once( ABSPATH . 'wp-admin/includes/file.php' );
		require_once( ABSPATH . 'wp-admin/includes/image.php' );
		define( 'WOO_PRODUCTS_PUBLISH', 'publish' );
		define( 'WOO_PRODUCTS_CONTENT_TYPE', 'product' );
		if ( isset( $_FILES['upload'] ) && ! $_FILES['upload']['error'] ) {
			if ( 'text/xml' === $_FILES['upload']['type'] ) {
				$file = $_FILES['upload'];
				$xml  = simplexml_load_file( $file['tmp_name'] );
				foreach ( $xml as $product ) {
					$post_id = wp_insert_post(
						array(
							'post_title'   => $product->title->__toString(),
							'post_content' => $product->content->__toString(),
							'post_status'  => WOO_PRODUCTS_PUBLISH,
							'post_type'    => WOO_PRODUCTS_CONTENT_TYPE,
						)
					);
					wp_set_object_terms( $post_id, 'simple', 'product_type' );
					wp_set_object_terms( $post_id, $product->categories->category->__toString(), 'product_cat' );
					update_post_meta( $post_id, '_visibility', $product->visibility->__toString() );
					update_post_meta( $post_id, '_stock_status', $product->stock_status->__toString() );
					update_post_meta( $post_id, 'total_sales', '0' );
					update_post_meta( $post_id, '_downloadable', 'no' );
					update_post_meta( $post_id, '_virtual', 'no' );
					update_post_meta( $post_id, '_regular_price', $product->regular_price->__toString() );
					update_post_meta( $post_id, '_sale_price', $product->sale_price->__toString() );
					update_post_meta( $post_id, '_purchase_note', '' );
					update_post_meta( $post_id, '_featured', 'no' );
					update_post_meta( $post_id, '_weight', '' );
					update_post_meta( $post_id, '_length', '' );
					update_post_meta( $post_id, '_width', '' );
					update_post_meta( $post_id, '_height', '' );
					update_post_meta( $post_id, '_sku', '' );
					update_post_meta( $post_id, '_product_attributes', array() );
					update_post_meta( $post_id, '_sale_price_dates_from', '' );
					update_post_meta( $post_id, '_sale_price_dates_to', '' );
					update_post_meta( $post_id, '_price', '' );
					update_post_meta( $post_id, '_sold_individually', '' );
					update_post_meta( $post_id, '_manage_stock', 'no' );
					update_post_meta( $post_id, '_backorders', 'no' );
					update_post_meta( $post_id, '_stock', $product->stock->__toString() );
					update_post_meta( $post_id, '_stock_status', $product->stock_status->__toString() );
					// Add image information.
					foreach ( $product->images as $p ) {
						$product_image_gallery_id = [];
						foreach ( $p->image as $img ) {
							$url   = $img->__toString();
							$image = media_sideload_image( $url, $post_id, '', 'src' );
							if ( $img->attributes()->__toString() ) {
								$thumbnail_id = get_attachment_id_from_src( $image );
							} else {
								$product_image_gallery_id[] = get_attachment_id_from_src( $image );
							}
						}
					}
					update_post_meta( $post_id, '_thumbnail_id', $thumbnail_id );
					update_post_meta( $post_id, '_download_limit', '' );
					update_post_meta( $post_id, '_download_expiry', '' );
					update_post_meta( $post_id, '_download_type', '' );
					update_post_meta( $post_id, '_product_image_gallery', implode( ',', $product_image_gallery_id ) );
					add_action( 'admin_notices', 'woo_products_admin_notice_success' );
				} // End foreach().
			} else {
				add_action( 'admin_notices', 'woo_products_admin_notice_error' );
			} // End if().
		} // End if().
	}
} catch ( Throwable $e ) {
	throw $e;
} // End try().
add_action( 'init', 'woo_products_xml_upload' );
