<div class="woo-upload-form" style="background:#cbc5d3; padding: 10px; margin-top: 40px; ">
    <h2><?php _e( 'Upload XML file for adding new products:' ); ?></h2>
    <form method="POST" enctype="multipart/form-data">
        <input type="file" name="upload" accept="text/xml"><br>
        <input type="submit" value="Import"
               style="background:#191e23; color:#fff8e5; margin-top: 20px; padding: 10px; font-weight: bold; cursor: pointer;">
    </form>
</div>
